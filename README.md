# Fonction-palindrome

Fonction très basique qui détermine si une chaine de caractères est oui ou non un palindrome (marche aussi avec des phrases complètes, les espaces sont ignorés)